# RULoaned

A Loan application prediction website where we can know whether a loan applicant's application will be accepted or not.

## Running it for the first time
```
git clone https://gitlab.com/compmonk/RULoaned.git
cd RULoaned
virtualenv venv
virtualenv -p /usr/bin/python3 venv
source venv/bin/activate
sudo apt-get install -y $(grep -vE "^\s*#" packages/packages.txt  | tr "\n" " ")
pip install -r packages/requirements.txt
python RULoaned/manage.py runserver --settings=RULoaned.settings.local
```



## To run
```
source venv/bin/activate
python RULoaned/manage.py runserver --settings=RULoaned.settings.local
